import collections
from bozor_types import *

def parse(expr):
   try:
      return NumC(float(expr))
   except (TypeError, ValueError):
      if expr == "true":
         return BoolC(true)
      elif expr == "false":
         return BoolC(false)
      elif not isinstance(expr, list):
         return IdC(expr)
      else:
         if expr[0] == "if":
            return IfC(parse(expr[1]), parse(expr[2]), parse(expr[3]))
         elif expr[0] == "with":
            bindings = expr[1:len(expr) - 1]
            paramNames = []
            paramVals = []
            for bindingNum in range (0, len(bindings)):
               if bindings[bindingNum][0] in paramNames:
                  raise SyntaxError("Duplicate binding in with")
               paramNames.append(bindings[bindingNum][0])
               paramVals.append(bindings[bindingNum][2])
            return AppC(LambdaC(paramNames, parse(expr[len(expr) - 1])), map(parse, paramVals))
         elif expr[0] == "fn":
            return LambdaC(expr[1], parse(expr[2]))
         elif isinstance(expr[0], collections.Hashable) and expr[0] in binop_functions:
            return BinopC(expr[0], parse(expr[1]), parse(expr[2]))
         else:
            return AppC(parse(expr[0]), map(parse, expr[1:len(expr)]))

def exprTok(strexp):
   if strexp[0] == "{":
      tokens = []
      braceCount = 0
      leftMatchNdx = -1

      for ndx, ch in enumerate(strexp):

         if braceCount == 1 and leftMatchNdx == -1 and not ch.isspace() and not ch == '}':
            leftMatchNdx = ndx

         if braceCount == 1 and ch.isspace() and leftMatchNdx != -1:
            tokens.append(exprTok(strexp[leftMatchNdx:ndx]))
            leftMatchNdx = -1

         if ch == '{':
            braceCount = braceCount + 1
         if ch == '}':
            braceCount = braceCount - 1

      if braceCount == 0:
         if leftMatchNdx != -1 and len(strexp) > 2:
            tokens.append(exprTok(strexp[leftMatchNdx:-1]))
      else:
         raise SyntaxError("Badly formed expression: Not enough closing braces")

      return tokens
   else:
      return strexp

def topEval(strexp):
   return parse(exprTok(strexp)).interp({})
