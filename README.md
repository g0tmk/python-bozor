##The python bozor3 implementation

[Presentation slides](https://docs.google.com/presentation/d/14heGHWEILX0a5yYtRQXLJSjs2zgoAspmpcudH2Qkrsc/edit?usp=sharing)

###testing
run `python test.py`

###examples:
#####types:

    >>> from bozor_types import *
    >>> a = NumC(1)
    >>> a.value
    1
    >>> str(a)
    '1'
    >>> a
    <bozor_types.NumC instance at 0x10cba8878>
    >>> isinstance(a, NumC)
    True
    >>> isinstance(a, ExprC)
    True


#####interp:

    >>> from bozor_types import *
    >>> a = NumC(1)
    >>> a.interp({})
    1
    >>> if_statement = IfC(BoolC(True), NumC(1), NumC(2))
    >>> if_statement.interp({})
    1

#####evaluation:

    >>> from parser import *
    >>> topEval("{+ 2 3}")
    5
    >>> topEval("{{fn {x} {+ 2 x}} 4}")
    6