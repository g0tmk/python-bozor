# Bozor types,


class ExprC():
    """this class is only here for inheritance checking"""
    pass

class NumC(ExprC):
    def __init__(self, num):
        if not isinstance(num, (int, long, float, complex)):
            raise TypeError("NumC value has to int, long, float, or complex")
        self.value = num

    def interp(self, env):
        return self.value

class IdC(ExprC):
    def __init__(self, id):
        self.id = id

    def interp(self, env):
        if self.id in env:
            return env[self.id]
        else:
            raise SyntaxError("Unbound variable '" + self.id + "'")


class BoolC(ExprC):
    def __init__(self, bool_val):
        if type(bool_val) is not bool:
            raise TypeError("BoolC value has to be boolean")
        self.value = bool_val

    def interp(self, env):
        return self.value


class IfC(ExprC):
    def __init__(self, cond_val, then_val, else_val):
        if not isinstance(cond_val, ExprC):
            raise TypeError("IfC condition must be expr")
        if not isinstance(then_val, ExprC):
            raise TypeError("IfC then clause must be expr")
        if not isinstance(else_val, ExprC):
            raise TypeError("IfC else clause must be expr")
        self.cond_val = cond_val
        self.then_val = then_val
        self.else_val = else_val

    def interp(self, env):
        if self.cond_val.interp(env):
            return self.then_val.interp(env)
        else:
            return self.else_val.interp(env)

class LambdaV():
   def __init__(self, params, body, env):
      self.params = params
      self.body = body
      self.env = env

   def interp(self, env):
      return "#<procedure>"

class LambdaC(ExprC):
   def __init__(self, params, body):
      self.params = params
      self.body = body

   def interp(self, env):
      return LambdaV(self.params, self.body, env)

class AppC(ExprC):
   def __init__(self, lam, argxs):
      if not isinstance(lam, LambdaC):
         raise TypeError("AppC must be called with a lambda")
      if not all(isinstance(arg, ExprC) for arg in argxs):
         raise TypeError("AppC must be called with a list of valid expressions")
      self.lam = lam
      self.argxs = argxs

   def interp(self, env):
      args = map(lambda x: x.interp(env), self.argxs)
      fn = self.lam.interp(env)
      lamEnv = fn.env.copy()

      if len(fn.params) != len(args):
         raise SyntaxError("Wrong arity")

      bindings = zip(fn.params, args)

      for p, v in bindings:
         lamEnv[p] = v

      return fn.body.interp(lamEnv)

binop_functions = {
   '+': lambda a, b: a + b,
   '-': lambda a, b: a - b,
   '*': lambda a, b: a * b,
   '/': lambda a, b: a / b,
   'eq?': lambda a, b: a == b,
   '<=' : lambda a, b: a <= b
}

class BinopC(ExprC):
   def __init__(self, operator, a, b):
      if not (isinstance(a, ExprC) and isinstance(b, ExprC)):
         raise TypeError("Operands to binary operator must be valid expressions")
      if operator not in binop_functions:
         raise SyntaxError("Undefined binary operator")
      self.operator = operator
      self.a = a
      self.b = b

   def interp(self, env):
      binop = binop_functions[self.operator]
      return binop(self.a.interp(env), self.b.interp(env))
