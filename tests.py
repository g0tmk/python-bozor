import unittest
from parser import *

class TestInterp(unittest.TestCase):
    def test_interp_nums(self):
        self.assertEqual(NumC(1).interp({}), 1)

    def test_interp_bools(self):
        self.assertEqual(BoolC(True).interp({}), True)
        self.assertEqual(BoolC(False).interp({}), False)

    def test_interp_ifs(self):
        true_if = IfC(BoolC(True), NumC(1), NumC(2))
        self.assertEqual(true_if.interp({}), 1)

        false_if = IfC(BoolC(False), NumC(1), NumC(2))
        self.assertEqual(false_if.interp({}), 2)


class TestTypes(unittest.TestCase):
    def setUp(self):
        self.numval = NumC(1)
        self.boolval = BoolC(True)

    def test_nums(self):
        self.assertEqual(self.numval.value, 1)

        self.assertTrue(isinstance(self.numval, NumC))
        self.assertTrue(isinstance(self.numval, ExprC))

        self.assertRaises(TypeError, NumC, "pants")

    def test_bools(self):
        self.assertEqual(self.boolval.value, True)
        self.assertEqual(BoolC(False).value, False)

        self.assertTrue(isinstance(self.boolval, BoolC))
        self.assertTrue(isinstance(self.boolval, ExprC))

        self.assertRaises(TypeError, BoolC, 6)

    def test_ifs(self):
        valid_if = IfC(self.boolval, self.numval, self.numval)

        self.assertRaises(TypeError, IfC, 6, self.numval, self.numval)
        self.assertRaises(TypeError, IfC, self.boolval, 6, self.numval)
        self.assertRaises(TypeError, IfC, self.boolval, self.numval, 6)

        self.assertTrue(isinstance(valid_if, IfC))
        self.assertTrue(isinstance(valid_if, ExprC))

class TestParse(unittest.TestCase):
   def tests(self):
      self.assertEqual(parse(['+', '2', '3']).interp({}), 5)

class TestTopEval(unittest.TestCase):
    def tests(self):
        self.assertEqual(topEval("{+ 2 {+ 5 1}}"), 8)
        self.assertEqual(topEval("{{fn {x} {+ x 2}} 3}"), 5)
        self.assertEqual(topEval("{with {z = 14} {y = 10} {+ z y}}"), 24)
        self.assertRaises(SyntaxError, topEval, "{with {z = 14} {z = 10} {+ z z}}")

class TestExprTok(unittest.TestCase):
    def test_exprTok(self):
        self.assertEqual(exprTok("{5   }"), ['5'])
        self.assertEqual(exprTok("{ {21} {22} }"), [['21'], ['22']])
        self.assertEqual(exprTok("{ 2 {a} b {}}"), ['2', ['a'], 'b', []])
        self.assertEqual(exprTok("{+ 2 3}"), ['+', '2', '3'])
        self.assertEqual(exprTok("{{fn {x} {+ x 2}} 3}"), [['fn', ['x'], ['+', 'x', '2']], '3'])


if __name__ == '__main__':
    #suite = unittest.TestLoader().loadTestsFromTestCase(TestTypes)
    #unittest.TextTestRunner(verbosity=2).run(suite)
    unittest.main(verbosity=2)
